package com.jixcayau.bookinventory.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jixcayau.bookinventory.models.BookModel;
import com.jixcayau.bookinventory.services.BookService;
import com.jixcayau.bookinventory.services.InventoryService;
import com.jixcayau.bookinventory.services.PricingService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class BookControllerTest {

    @Mock
    private BookService bookService;

    @Mock
    private InventoryService inventoryService;

    @Mock
    private PricingService pricingService;

    @InjectMocks
    private BookController bookController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(bookController).build();
    }

    @DisplayName("Get Books Success")
    @Test
    public void testGetBooksSuccess() throws Exception {
        List<BookModel> bookList = new ArrayList<>();
        bookList.add(new BookModel(1, "978-3-16", "Sample Book 1", "Author One", 1));
        bookList.add(new BookModel(2, "978-1-23", "Sample Book 2", "Author Two", 2));

        when(bookService.getBooks()).thenReturn(bookList);

        mockMvc.perform(get("/books")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @DisplayName("Get Books Failure")
    @Test
    public void testGetBooksFailure() throws Exception {
        when(bookService.getBooks()).thenThrow(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to get books"));

        mockMvc.perform(get("/books")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @DisplayName("Add Book Success")
    @Test
    public void testAddBookSuccess() throws Exception {
        BookModel book = new BookModel(1, "978-3-16", "Sample Book 1", "Author One", 1);
        ResponseEntity<BookModel> responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(book);

        when(bookService.addBook(any(BookModel.class))).thenReturn(responseEntity);

        mockMvc.perform(post("/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(book)))
                .andExpect(status().isCreated());
    }

    @DisplayName("Add Book Failure")
    @Test
    public void testAddBookFailure() throws Exception {
        BookModel book = new BookModel(1, "978-3-16", "Sample Book 1", "Author One", 1);

        when(bookService.addBook(any(BookModel.class))).thenThrow(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to add book"));

        mockMvc.perform(post("/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(book)))
                .andExpect(status().isInternalServerError());
    }

    @DisplayName("Remove Book Success")
    @Test
    public void testRemoveBookSuccess() throws Exception {
        when(bookService.removeBook(1)).thenReturn(true);

        mockMvc.perform(delete("/books/1"))
                .andExpect(status().isNoContent());
    }

    @DisplayName("Remove Book Failure")
    @Test
    public void testRemoveBookFailure() throws Exception {
        when(bookService.removeBook(1)).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Resource not found"));

        mockMvc.perform(delete("/books/1"))
                .andExpect(status().isNotFound());
    }

    @DisplayName("Update Book Success")
    @Test
    public void testUpdateBookSuccess() throws Exception {
        BookModel book = new BookModel(1, "978-3-16", "Sample Book 1", "Author One", 1);

        when(bookService.updateBook(1, book)).thenReturn(java.util.Optional.of(book));

        mockMvc.perform(put("/books/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(book)))
                .andExpect(status().isOk());
    }

    @DisplayName("Update Quantity Success")
    @Test
    public void testUpdateQuantitySuccess() throws Exception {
        when(inventoryService.updateQuantity(1, 5)).thenReturn(true);

        mockMvc.perform(put("/books/1/quantity")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("5"))
                .andExpect(status().isOk());
    }

    @DisplayName("Update Quantity Failure")
    @Test
    public void testUpdateQuantityFailure() throws Exception {
        when(inventoryService.updateQuantity(1, 5)).thenThrow(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to update quantity"));

        mockMvc.perform(put("/books/1/quantity")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("5"))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to update quantity"));
    }

    @DisplayName("Update Price Success")
    @Test
    public void testUpdatePriceSuccess() throws Exception {
        when(pricingService.updatePrice(1, 20.50)).thenReturn(true);

        mockMvc.perform(put("/books/1/price")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("20.50"))
                .andExpect(status().isOk());
    }

    @DisplayName("Update Price Failure")
    @Test
    public void testUpdatePriceFailure() throws Exception {
        when(pricingService.updatePrice(1, 20.50)).thenThrow(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to update price"));

        mockMvc.perform(put("/books/1/price")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("20.50"))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to update price"));
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

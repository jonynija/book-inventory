package com.jixcayau.bookinventory.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jixcayau.bookinventory.models.BookModel;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private static Integer bookId;

    @DisplayName("1. Add Book Success")
    @Test
    @Order(1)
    public void testAddBookSuccess() throws Exception {
        BookModel book = new BookModel();
        book.setISBN("ZZZZZZZZ");
        book.setTitle("Sample Book 1");
        book.setAuthor("Author One");
        book.setGenreId(1);
        book.setGenre("Fiction");
        book.setQuantity(10);
        book.setPrice(20.50);

        String response = mockMvc.perform(MockMvcRequestBuilders.post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value("Sample Book 1"))
                .andReturn().getResponse().getContentAsString();

        BookModel createdBook = objectMapper.readValue(response, BookModel.class);
        bookId = createdBook.getId();
    }

    @DisplayName("2. Get Books Success")
    @Test
    @Order(2)
    public void testGetBooksSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/books")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].title").value("Sample Book 1"));
    }

    @DisplayName("3. Update Book Success")
    @Test
    @Order(3)
    public void testUpdateBookSuccess() throws Exception {
        BookModel book = new BookModel();
        book.setId(bookId);
        book.setISBN("ZZZZZZZZ");
        book.setTitle("Updated Book");
        book.setAuthor("Updated Author");
        book.setGenreId(2);
        book.setGenre("Non-Fiction");
        book.setQuantity(10);
        book.setPrice(25.75);

        mockMvc.perform(MockMvcRequestBuilders.put("/books/" + bookId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value("Updated Book"));
    }

    @DisplayName("4. Update Quantity Success")
    @Test
    @Order(4)
    public void testUpdateQuantitySuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/books/" + bookId + "/quantity")
                .contentType(MediaType.APPLICATION_JSON)
                .content("5"))
                .andExpect(status().isOk());
    }

    @DisplayName("5. Update Price Success")
    @Test
    @Order(5)
    public void testUpdatePriceSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/books/" + bookId + "/price")
                .contentType(MediaType.APPLICATION_JSON)
                .content("30.50"))
                .andExpect(status().isOk());
    }

    @DisplayName("6. Remove Book Success")
    @Test
    @Order(6)
    public void testRemoveBookSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/books/" + bookId))
                .andExpect(status().isNoContent());
    }
}

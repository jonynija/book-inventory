package com.jixcayau.bookinventory.services;

import com.jixcayau.bookinventory.models.InventoryModel;
import com.jixcayau.bookinventory.repositories.IInventoryRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class InventoryServiceTest {

    @Mock
    IInventoryRepository repository;

    @InjectMocks
    InventoryService service;


    @DisplayName("Update Quantity Success")
    @Test
    public void updateQuantity_Success() {
        InventoryModel inventoryExpected = new InventoryModel();
        inventoryExpected.setId(1);
        inventoryExpected.setBookId(1);
        inventoryExpected.setQuantity(10);

        Mockito.when(repository.findByBookId(1))
                .thenReturn(inventoryExpected);

        Mockito.when(repository.save(inventoryExpected))
                .thenReturn(inventoryExpected);

        final boolean result = service.updateQuantity(1, 10);
        Assertions.assertTrue(result);

        Mockito.verify(repository, Mockito.times(1)).findByBookId(Mockito.anyInt());
        Mockito.verify(repository, Mockito.times(1)).save(Mockito.any());
    }

    @DisplayName("Update Quantity Failure")
    @Test
    public void updateQuantity_Failure() {
        Mockito.when(repository.findByBookId(1))
                .thenReturn(null);

        final boolean result = service.updateQuantity(1, 10);
        Assertions.assertFalse(result);

        Mockito.verify(repository, Mockito.times(1)).findByBookId(Mockito.anyInt());
        Mockito.verify(repository, Mockito.times(0)).save(Mockito.any());
    }

}
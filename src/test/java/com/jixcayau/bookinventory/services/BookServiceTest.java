package com.jixcayau.bookinventory.services;

import com.jixcayau.bookinventory.models.BookModel;
import com.jixcayau.bookinventory.repositories.IBookRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    IBookRepository repository;

    @InjectMocks
    BookService service;

    @DisplayName("Get Books Success")
    @Test
    public void getBooks_Success() {
        List<Object[]> results = new ArrayList<>();
        Object[] data1 = {1, "978-3-16", "Sample Book 1", "Author One", 1, "Fiction", 10, 20.50};
        Object[] data2 = {2, "978-1-23", "Sample Book 2", "Author Two", 2, "Non-Fiction", 15, 15.75};
        results.add(data1);
        results.add(data2);

        Mockito.when(repository.getBooksInfo()).thenReturn(results);

        List<BookModel> books = service.getBooks();
        Assertions.assertEquals(2, books.size());
        Assertions.assertEquals("Sample Book 1", books.get(0).getTitle());
        Assertions.assertEquals("Sample Book 2", books.get(1).getTitle());
    }

    @DisplayName("Get Books Failure")
    @Test
    public void getBooks_Failure() {
        Mockito.when(repository.getBooksInfo()).thenReturn(new ArrayList<>());

        List<BookModel> books = service.getBooks();
        Assertions.assertTrue(books.isEmpty());
    }

    @DisplayName("Add Book Success")
    @Test
    public void addBook_Success() {
        BookModel book = new BookModel();
        book.setId(1);
        book.setISBN("978-3-16");
        book.setTitle("Sample Book 1");
        book.setAuthor("Author One");
        book.setGenreId(1);
        book.setGenre("Fiction");
        book.setQuantity(10);
        book.setPrice(20.50);

        Mockito.when(repository.save(book)).thenReturn(book);

        ResponseEntity<BookModel> response = service.addBook(book);
        Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assertions.assertEquals(book, response.getBody());
    }

    @DisplayName("Add Book Failure")
    @Test
    public void addBook_Failure() {
        BookModel book = new BookModel();
        book.setId(1);
        book.setISBN("978-3-16");
        book.setTitle("Sample Book 1");
        book.setAuthor("Author One");
        book.setGenreId(1);
        book.setGenre("Fiction");
        book.setQuantity(10);
        book.setPrice(20.50);

        Mockito.when(repository.save(book)).thenThrow(new RuntimeException("Failed to save book"));

        RuntimeException exception = Assertions.assertThrows(RuntimeException.class, () -> service.addBook(book));
        Assertions.assertEquals("Failed to save book", exception.getMessage());
    }

    @DisplayName("Remove Book Success")
    @Test
    public void removeBook_Success() {
        Mockito.when(repository.findById(1)).thenReturn(Optional.of(new BookModel()));

        Assertions.assertDoesNotThrow(() -> service.removeBook(1));
    }

    @DisplayName("Remove Book Failure - Not Found")
    @Test
    public void removeBook_Failure_NotFound() {
        Mockito.when(repository.findById(1)).thenReturn(Optional.empty());

        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> service.removeBook(1));
        Assertions.assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
        Assertions.assertEquals("Resource not found", exception.getReason());
    }

    @DisplayName("Remove Book Failure - Internal Error")
    @Test
    public void removeBook_Failure_InternalError() {
        Mockito.when(repository.findById(1)).thenReturn(Optional.of(new BookModel()));
        Mockito.doThrow(new RuntimeException("Deletion failed")).when(repository).deleteBookAndDependencies(1);

        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> service.removeBook(1));
        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getStatusCode());
        Assertions.assertEquals("Error occurred while deleting the book", exception.getReason());
    }

    @DisplayName("Update Book Success")
    @Test
    public void updateBook_Success() {
        BookModel existingBook = new BookModel();
        existingBook.setId(1);
        existingBook.setTitle("Sample Book 1");
        existingBook.setAuthor("Author One");
        existingBook.setGenreId(1);

        BookModel updatedBook = new BookModel();
        updatedBook.setId(1);
        updatedBook.setTitle("Updated Book");
        updatedBook.setAuthor("Updated Author");
        updatedBook.setGenreId(2);

        Mockito.when(repository.findById(1)).thenReturn(Optional.of(existingBook));
        Mockito.when(repository.save(Mockito.any())).thenReturn(updatedBook);

        Optional<BookModel> result = service.updateBook(1, updatedBook);

        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals("Updated Book", result.get().getTitle());
        Assertions.assertEquals("Updated Author", result.get().getAuthor());
        Assertions.assertEquals(2, result.get().getGenreId());
    }

    @DisplayName("Update Book Failure - Not Found")
    @Test
    public void updateBook_Failure_NotFound() {
        BookModel updatedBook = new BookModel();
        updatedBook.setId(1);
        updatedBook.setTitle("Updated Book");
        updatedBook.setAuthor("Updated Author");
        updatedBook.setGenreId(2);

        Mockito.when(repository.findById(1)).thenReturn(Optional.empty());

        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> service.updateBook(1, updatedBook));
        Assertions.assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
        Assertions.assertEquals("Resource not found", exception.getReason());
    }

    @DisplayName("Update Book Failure - Internal Error")
    @Test
    public void updateBook_Failure_InternalError() {
        BookModel existingBook = new BookModel();
        existingBook.setId(1);
        existingBook.setTitle("Sample Book 1");
        existingBook.setAuthor("Author One");
        existingBook.setGenreId(1);

        BookModel updatedBook = new BookModel();
        updatedBook.setId(1);
        updatedBook.setTitle("Updated Book");
        updatedBook.setAuthor("Updated Author");
        updatedBook.setGenreId(2);

        Mockito.when(repository.findById(1)).thenReturn(Optional.of(existingBook));
        Mockito.when(repository.save(Mockito.any())).thenThrow(new RuntimeException("Update failed"));

        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> service.updateBook(1, updatedBook));
        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getStatusCode());
        Assertions.assertEquals("Error occurred while updating the book", exception.getReason());
    }
}

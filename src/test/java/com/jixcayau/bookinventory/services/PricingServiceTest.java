package com.jixcayau.bookinventory.services;

import com.jixcayau.bookinventory.models.PricingModel;
import com.jixcayau.bookinventory.repositories.IPricingRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PricingServiceTest {

    @Mock
    IPricingRepository repository;

    @InjectMocks
    PricingService service;

    @DisplayName("Update Price Success")
    @Test
    public void updatePrice_Success() {
        PricingModel pricingExpected = new PricingModel();
        pricingExpected.setId(1);
        pricingExpected.setBookId(1);
        pricingExpected.setPrice(20.50);

        Mockito.when(repository.findByBookId(1))
                .thenReturn(pricingExpected);

        Mockito.when(repository.save(pricingExpected))
                .thenReturn(pricingExpected);

        final boolean result = service.updatePrice(1, 20.50);
        Assertions.assertTrue(result);

        Mockito.verify(repository, Mockito.times(1)).findByBookId(Mockito.anyInt());
        Mockito.verify(repository, Mockito.times(1)).save(Mockito.any());
    }

    @DisplayName("Update Price Failure")
    @Test
    public void updatePrice_Failure() {
        Mockito.when(repository.findByBookId(1))
                .thenReturn(null);

        final boolean result = service.updatePrice(1, 20.50);
        Assertions.assertFalse(result);

        Mockito.verify(repository, Mockito.times(1)).findByBookId(Mockito.anyInt());
        Mockito.verify(repository, Mockito.times(0)).save(Mockito.any());
    }
}

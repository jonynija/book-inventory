package com.jixcayau.bookinventory.repositories;

import com.jixcayau.bookinventory.models.BookModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import jakarta.transaction.Transactional;


@Repository
public interface IBookRepository extends JpaRepository<BookModel, Integer> {
    @Modifying
    @Transactional
    @Query(value = "CALL deleteBookAndDependencies(?1)", nativeQuery = true)
    void deleteBookAndDependencies(Integer bookId);


    @Query(value = "CALL getBooksInfo()", nativeQuery = true)
    List<Object[]> getBooksInfo();
}

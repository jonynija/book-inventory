package com.jixcayau.bookinventory.repositories;

import com.jixcayau.bookinventory.models.InventoryModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IInventoryRepository extends JpaRepository<InventoryModel, Integer> {
    InventoryModel findByBookId(Integer bookId);
}

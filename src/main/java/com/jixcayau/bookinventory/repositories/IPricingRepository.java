package com.jixcayau.bookinventory.repositories;

import com.jixcayau.bookinventory.models.PricingModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPricingRepository extends JpaRepository<PricingModel, Integer> {
    PricingModel findByBookId(Integer bookId);
}

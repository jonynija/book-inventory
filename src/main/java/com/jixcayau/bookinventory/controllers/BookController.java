package com.jixcayau.bookinventory.controllers;

import com.jixcayau.bookinventory.models.BookModel;
import com.jixcayau.bookinventory.services.BookService;
import com.jixcayau.bookinventory.services.InventoryService;
import com.jixcayau.bookinventory.services.PricingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;
    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private PricingService pricingService;

    @GetMapping
    public List<BookModel> getBooks() {
    	
        return this.bookService.getBooks();
    }

    @PostMapping
    public ResponseEntity<BookModel> addBook(@RequestBody BookModel book) {
        return bookService.addBook(book);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> removeBook(@PathVariable("id") Integer id) {
        if (bookService.removeBook(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while deleting the book");
        }
    }

    @PutMapping(path = "/{id}")
    public Optional<BookModel> updateBook(@PathVariable("id") Integer id, @RequestBody BookModel book) {
        return bookService.updateBook(id, book);
    }

    @PutMapping("/{id}/quantity")
    public ResponseEntity<Void> updateQuantity(@PathVariable("id") Integer id, @RequestBody int quantity) {
        boolean isUpdated = inventoryService.updateQuantity(id, quantity);
        if (isUpdated) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/price")
    public ResponseEntity<Void> updatePrice(@PathVariable("id") Integer id, @RequestBody double price) {
        boolean isUpdated = pricingService.updatePrice(id, price);
        if (isUpdated) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
package com.jixcayau.bookinventory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookInventoryApplication {
	
	private static final Logger logger = LogManager.getLogger(BookInventoryApplication.class);

	public static void main(String[] args) {
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
		
		logger.debug("TEST");
		
		SpringApplication.run(BookInventoryApplication.class, args);
	}
}
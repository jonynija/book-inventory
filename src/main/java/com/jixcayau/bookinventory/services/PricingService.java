package com.jixcayau.bookinventory.services;

import com.jixcayau.bookinventory.models.PricingModel;
import com.jixcayau.bookinventory.repositories.IPricingRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PricingService {

    @Autowired
    IPricingRepository pricingRepository;

    public boolean updatePrice(Integer bookId, double price) {
        PricingModel pricing = pricingRepository.findByBookId(bookId);
        if (pricing != null) {
            pricing.setPrice(price);
            pricingRepository.save(pricing);
            return true;
        }
        return false;
    }
}
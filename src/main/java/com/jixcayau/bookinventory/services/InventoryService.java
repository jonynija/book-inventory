package com.jixcayau.bookinventory.services;

import com.jixcayau.bookinventory.models.InventoryModel;
import com.jixcayau.bookinventory.repositories.IInventoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryService {

    @Autowired
    IInventoryRepository inventoryRepository;


    public boolean updateQuantity(Integer bookId, int quantity) {
        InventoryModel inventory = inventoryRepository.findByBookId(bookId);
        if (inventory != null) {
            inventory.setQuantity(quantity);
            inventoryRepository.save(inventory);
            return true;
        }
        return false;
    }
}
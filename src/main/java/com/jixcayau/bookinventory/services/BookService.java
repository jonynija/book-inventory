package com.jixcayau.bookinventory.services;

import com.jixcayau.bookinventory.models.BookModel;
import com.jixcayau.bookinventory.repositories.IBookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class BookService {

    @Autowired
    IBookRepository bookRepository;

    public List<BookModel> getBooks() {
        List<Object[]> results = bookRepository.getBooksInfo();
        List<BookModel> books = new ArrayList<>();

        for (Object[] result : results) {
            BookModel book = new BookModel();
            book.setId((Integer) result[0]);
            book.setISBN((String) result[1]);
            book.setTitle((String) result[2]);
            book.setAuthor((String) result[3]);
            book.setGenreId((Integer) result[4]);
            book.setGenre((String) result[5]);
            book.setQuantity((Integer) result[6]);
            book.setPrice((Double) result[7]);

            books.add(book);
        }

        return books;
    }

    public ResponseEntity<BookModel> addBook(BookModel book) {
        return new ResponseEntity<BookModel>(bookRepository.save(book), HttpStatus.CREATED);
    }

    public boolean removeBook(Integer id) {
        Optional<BookModel> optionalBook = bookRepository.findById(id);

        if (optionalBook.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Resource not found");
        }

        try {
            bookRepository.deleteBookAndDependencies(id);
            return true;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while deleting the book");
        }
    }

    public Optional<BookModel> updateBook(Integer id, BookModel bookDetails) {
        Optional<BookModel> optionalBook = bookRepository.findById(id);

        if (optionalBook.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Resource not found");
        }

        BookModel existingBook = optionalBook.get();

        existingBook.setTitle(bookDetails.getTitle());
        existingBook.setAuthor(bookDetails.getAuthor());
        existingBook.setGenreId(bookDetails.getGenreId());

        try {
            return Optional.of(bookRepository.save(existingBook));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while updating the book");
        }
    }
}